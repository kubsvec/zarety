from io import BytesIO

import requests
from openpyxl import Workbook
from openpyxl import load_workbook

wb = load_workbook(filename=BytesIO(requests.get('https://435228.myshoptet.com/user/documents/upload/aussiebum-swimwear-final.xlsx').content))
ws = wb['All product']

wb_d = Workbook()
target = wb_d.active
target.title = 'Savannah Rulezz'

first = True

target_row = 2
for row in ws.iter_rows():
    if first:
        for cell in row:
            if cell.column <= 3:
                target[cell.coordinate] = cell.value

            elif cell.column > 28:
                target.cell(cell.row, cell.column - 23, value=cell.value)

        target['D1'] = 'velikost'
        target['E1'] = 'barva'
        first = False
        continue

    code = row[0].value

    sizes = []
    colors = []

    for cell in row:
        # if cell.col_idx
        if 4 <= cell.column <= 15 and cell.value:
            sizes.append(cell.value)

        if 16 <= cell.column <= 28 and cell.value:
            colors.append(cell.value)

    if not sizes:
        print('ERROR, no sizes for product %d' % code)
        continue
    if not colors:
        print('WARN, no colors for product %d' % code)
        colors = ['']

    for size in sizes:
        for color in colors:
            target.cell(target_row, 1, '%s-%s-%s' % (code, size, color))
            target.cell(target_row, 2, row[1].value)
            target.cell(target_row, 3, row[2].value)
            target.cell(target_row, 4, size)
            target.cell(target_row, 5, color)

            for cell in row:
                if cell.column > 28:
                    target.cell(target_row, cell.column - 23, cell.value)

            target_row += 1

wb_d.save(filename='result.xlsx')
