from io import BytesIO

import requests
from openpyxl import Workbook
from openpyxl import load_workbook

wb = load_workbook(filename=BytesIO(requests.get('https://435228.myshoptet.com/user/documents/upload/aussiebum-swimwear-final.xlsx').content))
# wb = load_workbook(filename='aussiebum-swimwear-final.xlsx')

ws = wb[wb.sheetnames[0]]

wb_d = Workbook()
target = wb_d.active
target.title = 'Updated'

target_row = 2
first = True
for row in ws.iter_rows():
    if first:
        first = False
        for cell in row:
            target.cell(1, cell.column, cell.value)
        target.cell(1, 16, 'teimgroup_id')
        target.cell(1, 17, 'product-id')
        target.cell(1, 18, 'varianta')
        continue

    for suffix in ['S', 'M', 'L', 'XL', 'XXL']:
        for i in range(15):
            target.cell(target_row, row[i].column, row[i].value)

        target.cell(target_row, 16, row[15].value)
        target.cell(target_row, 17, '%s-%s' % (row[15].value, suffix))
        target.cell(target_row, 18, suffix)

        target_row += 1

wb_d.save(filename='result.xlsx')
